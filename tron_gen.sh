#!/bin/bash

TRON=tron.html

./html_header.py > ${TRON}
./html_table_from_csv.py tron.csv >> ${TRON}
sed -i "s/XXXXXX/☆/" ${TRON}

sudo cp ${TRON} /var/www/html/index.html
sudo cp tron.css /var/www/html/

exit 0

#!/usr/bin/python2

import csv
import sys

from sys import stdout

csv_file = open(sys.argv[1], 'rb')
reader = csv.DictReader(csv_file, delimiter='~')

if reader is None or reader.fieldnames is None or len(reader.fieldnames) == 0:
    exit()

def thead_tfoot(fieldnames):
    str_ret = ""
    for hdr in fieldnames:
        str_ret += "      <th>" + hdr + "</th>\n"
    return str_ret

header = thead_tfoot(reader.fieldnames)

## thead + tfoot
stdout.write("<table>\n  <thead>\n    <tr>\n" + header + "      <th>Note</th>\n")
stdout.write("</tr>\n  </thead>\n  <tfoot>\n    <tr>\n" \
             + header + "      <th>Note</th>    </tr>\n  </tfoot>\n")

## tbody
for row in reader:
    stdout.write("  <tbody>\n    <tr>\n")
    for hdr in reader.fieldnames:
        stdout.write('      <td>' + row[hdr] + '</td>\n')
    stdout.write("    <td>\n");
    stdout.write("     <div class=\"rating\"><!--\n")
    stdout.write("       --><a href=\"#5\" title=\"5 starts\">XXXXXX</a><!--\n");
    stdout.write("       --><a href=\"#4\" title=\"4 starts\">XXXXXX</a><!--\n");
    stdout.write("       --><a href=\"#3\" title=\"3 starts\">XXXXXX</a><!--\n");
    stdout.write("       --><a href=\"#2\" title=\"2 starts\">XXXXXX</a><!--\n");
    stdout.write("       --><a href=\"#1\" title=\"1 starts\">XXXXXX</a>\n");
    stdout.write("     </div>\n");
    stdout.write("    </td>\n");
    stdout.write("    </tr>\n  </tbody>\n")

stdout.write("</table>")

#!/usr/bin/python2

import sys

from sys import stdout
from sys import argv

stdout.write(
"""
<head>
  <link rel="stylesheet" type="text/css" href="tron.css" media="screen" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
""")

if len(argv) == 2 and argv[1] == "refresh":
    stdout.write(
    """
    <meta http-equiv="refresh" content="5">
    """)

stdout.write(
"""
</head>

<body onload="onLoad();" onresize="onResize();">
""")

#  <script src="http://simile.mit.edu/timeline/api/timeline-api.js" type="text/javascript"></script>
#  <script src="timeline.js" type="text/javascript"></script>
